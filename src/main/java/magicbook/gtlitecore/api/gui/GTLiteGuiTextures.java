package magicbook.gtlitecore.api.gui;

import gregtech.api.gui.resources.SteamTexture;
import gregtech.api.gui.resources.TextureArea;

public class GTLiteGuiTextures {

    //  Steam Overlay
    public static final SteamTexture FLUID_SLOT_STEAM = SteamTexture.fullImage("textures/gui/base/fluid_slot_%s.png");
    public static final SteamTexture INT_CIRCUIT_OVERLAY_STEAM = SteamTexture.fullImage("textures/gui/progress_bar/int_circuit_overlay_%s.png");

    //  Progress Bar
    public static final TextureArea PROGRESS_BAR_NANOSCALE = TextureArea.fullImage("textures/gui/progress_bar/progress_bar_nanoscale.png");
    public static final TextureArea PROGRESS_BAR_COMPONENT_ASSEMBLY_LINE_1 = TextureArea.fullImage("textures/gui/progress_bar/progress_bar_component_assembly_line_1.png");
    public static final TextureArea PROGRESS_BAR_COMPONENT_ASSEMBLY_LINE_2 = TextureArea.fullImage("textures/gui/progress_bar/progress_bar_component_assembly_line_2.png");
    public static final TextureArea PROGRESS_BAR_VIRTUAL_COSMOS_SIMULATOR = TextureArea.fullImage("textures/gui/progress_bar/progress_bar_virtual_cosmos_simulator.png");
    public static final TextureArea PROGRESS_BAR_HEAT_EXCHANGE = TextureArea.fullImage("textures/gui/progress_bar/progress_bar_heat_exchange.png");

    //  Overlay
    public static final TextureArea FOIL_OVERLAY = TextureArea.fullImage("textures/gui/overlay/foil_overlay.png");
    public static final TextureArea NANOSCALE_OVERLAY_1 = TextureArea.fullImage("textures/gui/overlay/nanoscale_overlay_1.png");
    public static final TextureArea NANOSCALE_OVERLAY_2 = TextureArea.fullImage("textures/gui/overlay/nanoscale_overlay_2.png");
    public static final TextureArea DISH_OVERLAY = TextureArea.fullImage("textures/gui/overlay/dish_overlay.png");

    //  Button
    public static final TextureArea BUTTON_STRUCTURE_SWITCHING = TextureArea.fullImage("textures/gui/widget/button_structure_switching.png");
    public static final TextureArea BUTTON_YOTTA_MODE = TextureArea.fullImage("textures/gui/widget/button_yotta_mode.png");
    public static final TextureArea FUSION_REACTOR_MK4_TITLE = TextureArea.fullImage("textures/gui/widget/fusion_reactor_mk4_title.png");
    public static final TextureArea FUSION_REACTOR_MK5_TITLE = TextureArea.fullImage("textures/gui/widget/fusion_reactor_mk5_title.png");

    //  Logo
    public static final TextureArea SPACE_ELEVATOR_LOGO = TextureArea.fullImage("textures/gui/icon/space_elevator_logo.png");
    public static final TextureArea SPACE_ELEVATOR_LOGO_DARK = TextureArea.fullImage("textures/gui/icon/space_elevator_logo_dark.png");

    public GTLiteGuiTextures() {}
}