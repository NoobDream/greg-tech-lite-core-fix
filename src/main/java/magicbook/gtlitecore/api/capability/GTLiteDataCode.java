package magicbook.gtlitecore.api.capability;

public class GTLiteDataCode {
    public static final int ChannelPreciseAssembler1 = 9900;
    public static final int ChannelPreciseAssembler2 = 9901;
    public static final int ChannelReinforcedRotorHolder = 9902;
    public static final int ChannelMegaTurbine = 9903;
    public static final int ChannelHeatExchangerHeat = 9904;
    public static final int ChannelHeatExchangerSuperheat = 9905;
    public static final int ChannelHeatExchangerRate = 9906;
}
