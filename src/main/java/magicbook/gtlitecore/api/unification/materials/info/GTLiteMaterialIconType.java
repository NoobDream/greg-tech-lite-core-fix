package magicbook.gtlitecore.api.unification.materials.info;

import gregtech.api.unification.material.info.MaterialIconType;

public class GTLiteMaterialIconType {
    public static final MaterialIconType milled = new MaterialIconType("milled");
    public static final MaterialIconType seedCrystal = new MaterialIconType("seedCrystal");
    public static final MaterialIconType boule = new MaterialIconType("boule");
    public static final MaterialIconType swarm = new MaterialIconType("swarm");
    public static final MaterialIconType nanotube = new MaterialIconType("nanotube");
    public static final MaterialIconType nanosensor = new MaterialIconType("nanosensor");
    public static final MaterialIconType singularity = new MaterialIconType("singularity");
}